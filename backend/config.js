const port = 5000;
const dbUrl = 'mongodb://localhost/brainhub'

module.exports = {port, dbUrl};
