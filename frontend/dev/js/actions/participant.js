export function participantFetchError(error) {
    return {
        type: 'PARTICIPANT_ERROR',
        payload: {error}
    };
}

export function participantLoading() {
    return {
        type: 'PARTICIPANT_LOADING',
    };
}

export function participantFetchSuccess(participant) {
    return {
        type: 'PARTICIPANT_SUCCESS',
        payload: {participant: participant}
    };
}

export function participantClear() {
    return {
        type: 'PARTICIPANT_CLEAR',
    };
}

export function participantFetch(url, id) {
    return (dispatch) => {
        dispatch(participantLoading());
        fetch(url + "/" + id)
            .then((response) => {
                if (!response.ok) {
                    return new Error("Can't load data!");
                }
                return response;
            })
            .then((response) => response.json())
            .then((items) => dispatch(participantFetchSuccess(items)))
            .catch(() => dispatch(participantFetchError("Can't load data!")));
    };
}

export function participantOnValueChange({field, value}) {
    return {
        type: 'PARTICIPANT_VALUE_CHANGE',
        payload: {field, value}
    };
}

export function participantSaveError(error) {
    return {
        type: 'PARTICIPANT_SAVE_ERROR',
        payload: {error}
    };
}

export function participantSaving() {
    return {
        type: 'PARTICIPANT_SAVING',
    };
}

export function participantSaved(participant) {
    return {
        type: 'PARTICIPANT_SAVED',
    };
}

export function participantSave(url, participant) {
    const method = participant._id ? "PUT" : "POST";
    const _url = participant._id ? url + "/" + participant._id : url;
    return (dispatch) => {
        dispatch(participantSaving());
        fetch(_url, {
            method,
            body: JSON.stringify(participant)
        }).then((response) => {
            if (!response.ok) {
                return new Error("Can't load data!");
            }
            return response;
        }).then((response) => response.json())
            .then((items) => dispatch(participantSaved(items)))
            .catch(e => {console.log(e), dispatch(participantSaveError("Participant can't be saved"))});
    };
}
export function participantDeleteError(error) {
    return {
        type: 'PARTICIPANT_DELETE_ERROR',
        payload: {error}
    };
}

export function participantDeleting() {
    return {
        type: 'PARTICIPANT_DELETING',
    };
}

export function participantDeleted(participant) {
    return {
        type: 'PARTICIPANT_DELETED',
    };
}

export function participantDelete(url, participant) {
    const _url = participant._id ? url + "/" + participant._id : url;
    return (dispatch) => {
        dispatch(participantDeleting());
        fetch(_url, {
            method: "DELETE",
        }).then((response) => {
            if (!response.ok) {
                return new Error("Can't load data!");
            }
            return response;
        }).then((response) => response.json())
            .then((items) => dispatch(participantDeleted(items)))
            .catch(() => dispatch(participantDeleteError("Can't load data!")));
    };
}

export function participantAddValidationError(field, msg) {
    return {
        type: 'PARTICIPANT_ADD_VALIDATION_ERROR',
        payload:{field, msg}
    };
}

export function participantClearValidationErrors() {
    return {
        type: 'PARTICIPANT_CLEAR_VALIDATION_ERRORS',
    };
}



