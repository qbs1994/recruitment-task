const dbUrl = require('../config').dbUrl;
const chai = require('chai');
const app = require('../index');

const Participant = require('../app/models/participant');
describe("Models tests", function () {
    it(`Connect to mongoose`, function (done) {
        const mongoose = require('mongoose');
        mongoose.createConnection(dbUrl);
        done()
    })
    it(`Add new participant`, function (done) {
        const participant = new Participant({
            FirstName: "Jan",
            LastName: "Nowak",
            Email: "jan@nowak.pl",
            EventDate: new Date()
        })
        participant.save().then((res) => {
            chai.expect(typeof res._id).to.be.a('string');
            return Participant.findOne({FirstName: "Jan", LastName: "Nowak"})
        }).then(participant => {
            chai.expect(typeof participant._id).to.be.a('string');
            done()
        }).catch(err => {
            throw err
        })
    });
    it(`Edit participant`, function (done) {
        let _id;
        Participant.findOne({FirstName: "Jan", LastName: "Nowak"})
            .then(participant => {
                _id = participant._id;
                chai.expect(typeof _id).to.be.a('string');
                return Participant.update({_id}, {$set: {LastName: "Kowalski", Email: "jan@kowalski.pl"}})
            }).then(res => {
            return Participant.findOne({_id})
        }).then(participant => {
            chai.assert.equal(participant.LastName, "Kowalski");
            chai.assert.equal(participant.Email, "jan@kowalski.pl");
            done()
        }).catch(err => {
            throw err
        })
    });
    it(`Remove new participant`, function (done) {
        let _id;
        Participant.findOne({FirstName: "Jan", LastName: "Kowalski"})
            .then(participant => {
                _id = participant._id;
                chai.expect(typeof _id).to.be.a('string');
                return Participant.remove({_id})
            }).then(res => {
            return Participant.findOne({_id})
        }).then(res => {
            chai.expect(res).to.be.null;
            done()
        }).catch(err => {
            throw err
        })
    });
});
