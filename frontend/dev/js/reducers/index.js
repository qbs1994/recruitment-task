import {combineReducers} from 'redux';
import ParticipantReducer from './reducer-participant';
import ParticipantsListReducer from './reducer-participants-list';

const allReducers = combineReducers({
    participant: ParticipantReducer,
    participantsList: ParticipantsListReducer,
});

export default allReducers
