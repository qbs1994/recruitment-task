Recruitment task application
============================

Application is separated into two directories:  `backend` and `frontend`. Both apps run independent.

Running apps
-------

 1. Make sure that MongoDb server is running. Default base is called `brainhub`. Connection url can be modified in `backend/config.js`
 2. Go to `backend` folder and run `npm install` and `npm start`. Application backend should be running on port `5000`.

> Port can be modified in `backend/config.js` , but you must remember to change proxy configuration in `webpack.config.js` in `frontend` folder.

 3. In second terminal go to `frontend` folder and run `npm install` and `npm start`. Webpack server should run app on `3000` port. It can be changed in `webpack.config.js` file.

> Webpack proxy should forward api to webpack server.

Api should send status json on `http://localhost:3000/api`
Application should run on `http://localhost:3000/`

Tests
-------
To run tests go to `backend` folder and run `npm run test`