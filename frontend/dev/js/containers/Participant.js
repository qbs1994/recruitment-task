import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    participantFetch,
    participantOnValueChange,
    participantSave,
    participantClear,
    participantAddValidationError,
    participantClearValidationErrors,
    participantDelete
} from '../actions/index'
import {Link} from 'react-router-dom'
import Loader from '../components/Loader'

class Participant extends Component {
    componentDidMount() {
        this.props.match.params.id ? this.props.participantFetch("/api/participant", this.props.match.params.id) : this.props.participantClear()
    }

    onValueChange(e) {
        e.preventDefault();
        this.props.participantOnValueChange({field: e.target.name, value: e.target.value})
    }

    onFormSubmit(e) {
        e.preventDefault();
        this.props.participantClearValidationErrors()
        if (this.validateForm(this.props.participant.participant)) {
            this.props.participantSave("/api/participant", this.props.participant.participant)
        }
    }

    onParticipantDelete(e) {
        e.preventDefault();
        this.props.participantDelete("/api/participant", this.props.participant.participant)
    }

    validateForm(participant) {
        let valid = true;
        if (!participant.FirstName || participant.FirstName.length < 1) {
            this.props.participantAddValidationError("FirstName", "Please fill First Name field")
            valid = false
        }
        if (!participant.LastName || participant.LastName.length < 1) {
            this.props.participantAddValidationError("LastName", "Please fill Last Name field")
            valid = false
        }
        const emailRe = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (!participant.Email || participant.Email.length < 1) {
            this.props.participantAddValidationError("Email", "Please fill Email field")
            valid = false
        } else if (!emailRe.test(participant.Email)) {
            this.props.participantAddValidationError("Email", "Email not valid")
            valid = false
        }
        return valid
    }

    renderParticipantForm(participant) {
        return <form>

            <div className="form-group">
                <label>First Name</label>
                <input className="form-control"
                       required
                       type="text"
                       value={participant.participant.FirstName}
                       name="FirstName"
                       onChange={e => this.onValueChange(e)}/>
                {participant.validationErrors && participant.validationErrors.FirstName ?
                    <small className="form-text text-danger">{participant.validationErrors.FirstName}</small> : null}
            </div>

            <div className="form-group">
                <label>Last Name</label>
                <input className="form-control"
                       required
                       type="text"
                       value={participant.participant.LastName}
                       name="LastName"
                       onChange={e => this.onValueChange(e)}/>
                {participant.validationErrors && participant.validationErrors.LastName ?
                    <small className="form-text text-danger">{participant.validationErrors.LastName}</small> : null}
            </div>

            <div className="form-group">
                <label>Email</label>
                <input className="form-control"
                       required
                       type="email"
                       value={participant.participant.Email}
                       name="Email"
                       onChange={e => this.onValueChange(e)}/>
                {participant.validationErrors && participant.validationErrors.Email ?
                    <small className="form-text text-danger">{participant.validationErrors.Email}</small> : null}
            </div>

            <div className="form-group">
                <label>Date</label>
                <input className="form-control"
                       required
                       type="date"
                       value={new Date(participant.participant.EventDate).toISOString().slice(0, 10)}
                       name="EventDate"
                       onChange={e => this.onValueChange(e)}/>
                {participant.validationErrors && participant.validationErrors.EventDate ?
                    <small className="form-text text-danger">{participant.validationErrors.EventDate}</small> : null}
            </div>
            <div className="row">
                <div className="col-lg-6">
                    <input className="btn btn-primary"
                           type="submit"
                           value={participant.status === "SUCCESS" ? "Save" : "Add"}
                           onClick={e => this.onFormSubmit(e)}/>
                </div>
                <div className="col-lg-6 text-right">
                    {participant.status === "SUCCESS" &&
                    <button className="btn btn-danger"
                            onClick={e => this.onParticipantDelete(e)}>Delete
                    </button>

                    }
                </div>
            </div>


        </form>
    }

    renderAlert(type, msg) {
        return <div className="col-lg-12">
            <div className={"my-3 alert alert-" + type}>{msg}</div>
            <Link to="/">
                <button className="btn btn-info">Ok</button>
            </Link>
        </div>
    }

    render() {
        return <div className="row">
            <div className="col-lg-12 text-right">
                <Link to="/">
                    <button className="btn btn-info">Back</button>
                </Link>
            </div>
            {
                (() => {
                    if (this.props.participant.status === "DELETED") {
                        return this.renderAlert("warning", "Participant deleted")
                    }
                    if (this.props.participant.status === "ERROR") {
                        return this.renderAlert("danger", "Error - " + this.props.participant.msg)
                    }
                    else if (this.props.participant.status === "SAVED") {
                        return this.renderAlert("success", "Saved - " + this.props.participant.msg)
                    }
                    else if (this.props.participant.status === "SUCCESS" || this.props.participant.status === "NEW") {
                        return <div className="col-lg-12">
                            {this.renderParticipantForm(this.props.participant)}
                        </div>
                    }
                    else {
                        return <Loader/>
                    }
                })()
            }
        </div>


    }
}

function mapStateToProps(state) {
    return {
        participant: state.participant
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        participantFetch,
        participantOnValueChange,
        participantSave,
        participantClear,
        participantAddValidationError,
        participantClearValidationErrors,
        participantDelete
    }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Participant);
