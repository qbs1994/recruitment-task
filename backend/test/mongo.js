const dbUrl =  require('../config').dbUrl;
const chai = require('chai');

describe("Database tests", function() {
    describe("Test Database setup", function() {
        it(`connect to ${dbUrl} database`, function(done) {
            const MongoClient = require('mongodb').MongoClient;
            MongoClient.connect( dbUrl, function(err, db) {
                chai.assert(db.serverConfig.isConnected());
                db.close();
                done()
            });
        });
        it(`clear participants in ${dbUrl} database`, function(done) {
            const MongoClient = require('mongodb').MongoClient;
            MongoClient.connect( dbUrl, function(err, db) {
                db.collection("participants").remove();
                db.close();
                done()
            });
        });
    });
});
