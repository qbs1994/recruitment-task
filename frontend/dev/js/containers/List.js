import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {participantsListFetch} from '../actions/index'
import Loader from "../components/Loader"

class List extends Component {

    componentDidMount() {
        this.props.participantsListFetch("/api/participants")
    }

    renderAlert(type, msg) {
        return <div className="col-lg-12">
            <div className={"my-3 alert alert-" + type}>{msg}</div>
            <Link to="/">
                <button className="btn btn-info">Ok</button>
            </Link>
        </div>
    }

    renderList() {
        return <table className="table table-bordered">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Event Date</th>
            </tr>
            </thead>
            <tbody>
            {
                this.props.participantsList.participants.map((participant, i) => {
                    return (
                        <tr key={i}>
                            <td>{participant.FirstName}</td>
                            <td>{participant.LastName}</td>
                            <td>{participant.Email}</td>
                            <td>{new Date(participant.EventDate).toDateString()}</td>
                            <td><Link to={"/participant/" + participant._id}>Edit</Link></td>
                        </tr>
                    );
                })
            }
            </tbody>
        </table>
    }

    render() {
        return (
            (() => {
                if (this.props.participantsList.status === "ERROR") {
                    return this.renderAlert("danger", "Error - " + this.props.participantsList.msg)
                }
                else if (this.props.participantsList.status === "SUCCESS") {
                    return <div className="text-center">
                        {this.renderList()}
                        <Link to="/participant" className="btn btn-info">Add new participant</Link>
                    </div>
                }
                else {
                    return <Loader/>
                }
            })()


        );
    }

}

function mapStateToProps(state) {
    return {
        participantsList: state.participantsList
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({participantsListFetch}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(List);
