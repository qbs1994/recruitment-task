const express = require('express')
const router = express.Router()

const routes = require('./routes')

router.route("/")
    .get(routes.root)

router.route("/participants")
    .get(routes.getParticipants)

router.route("/participant/")
    .post(routes.postParticipant)

router.route("/participant/:id")
    .get(routes.getParticipant)
    .post(routes.postParticipant)
    .delete(routes.deleteParticipant)
    .put(routes.updateParticipant);

module.exports = router