const defaultParticipant = {FirstName: "", LastName: "", Email: "", EventDate: new Date()}

export default function (state = {isLoading:false, success: false, error: false, participant: defaultParticipant}, action) {
    switch (action.type) {
        case 'PARTICIPANT_LOADING':
            return {status:"LOADING", msg: "Participant loading", participant: defaultParticipant};
            break;
        case 'PARTICIPANT_ERROR':
            return {status:"ERROR", msg: action.payload.error, participant: defaultParticipant};
            break;
        case 'PARTICIPANT_SUCCESS':
            return {status:"SUCCESS", msg: "Participant Loaded", participant: action.payload.participant};
            break;

        case 'PARTICIPANT_VALUE_CHANGE':
            return {...state, participant: {...state.participant, [action.payload.field]: action.payload.value}};
            break;

        case 'PARTICIPANT_SAVING':
            return {status:"LOADING", msg: "Participant saving", participant: state.participant};
            break;
        case 'PARTICIPANT_SAVE_ERROR':
            return {status:"ERROR", msg: action.payload.error, participant: state.participant};
            break;
        case 'PARTICIPANT_SAVED':
            return {status:"SAVED", msg: "Participant saved", participant: state.participant};
            break;

        case 'PARTICIPANT_DELETING':
            return {status:"LOADING", msg: "Participant deleting", participant: {}};
            break;
        case 'PARTICIPANT_DELETE_ERROR':
            return {status:"ERROR", msg: action.payload.error, participant: {}};
            break;
        case 'PARTICIPANT_DELETED':
            return {status:"DELETED", msg: "Participant deleted", participant: {}};
            break;

        case 'PARTICIPANT_CLEAR':
            return {status: "NEW", msg: "", participant: defaultParticipant, validationErrors:{}};
            break;

        case 'PARTICIPANT_ADD_VALIDATION_ERROR':
            return {...state, validationErrors: {...state.validationErrors, [action.payload.field]: action.payload.msg  }};
            break;
        case 'PARTICIPANT_CLEAR_VALIDATION_ERRORS':
            return {...state, validationErrors: {}};
            break;
        default:
            return state
    }
}