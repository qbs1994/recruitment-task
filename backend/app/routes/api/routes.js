const mongoose = require('mongoose');
const Participant = require('../../models/participant');


function root(req, res) {
    res.json({status: "ok"});
}

function getParticipants(req, res) {
    const query = Participant.find({});
    query.exec((err, participants) => {
        if (err) res.send(err);
        res.json(participants);
    });
}

function postParticipant(req, res) {
    const data = typeof req.body === "object" ? req.body : JSON.parse(req.body);
    const newParticipant = new Participant(data);
    newParticipant.save((err, participant) => {
        if (err) {
            res.status(400).send(err)
        }
        else {
            res.json({message: "Participant successfully added!", participant});
        }
    });
}

function getParticipant(req, res) {
    Participant.findById(req.params.id, (err, participant) => {
        if (err) {
            res.status(500).send(err)
        } else if (!participant) {
            res.status(404).json(participant);
        } else {
            res.json(participant);
        }
    });
}

function deleteParticipant(req, res) {
    Participant.remove({_id: req.params.id}, (err, result) => {
        res.json({message: "Participant successfully deleted!", result});
    });
}

function updateParticipant(req, res) {
    const data = typeof req.body === "object" ? req.body : JSON.parse(req.body);
    Participant.findById({_id: req.params.id}, (err, participant) => {
        if (err) {
            res.status(404).send(err)
        } else {
            Participant.update({_id: participant._id}, {$set: data}, {
                upsert: true,
                runValidators: true
            }, (err, status) => {
                if (err) {
                    res.status(400).send(err)
                } else {
                    res.json({message: 'Participant updated!', status});
                }
            });
        }
    });
}

module.exports = {getParticipants, postParticipant, getParticipant, deleteParticipant, updateParticipant, root};