let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let ParticipantSchema = new Schema(
    {
        FirstName: { type: String, required: true, minlength: 1 },
        LastName: { type: String, required: true, minlength: 1 },
        Email: { type: String, required: true, minlength: 1, match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]},
        EventDate: { type: Date, required: true},
        createdAt: { type: Date, default: Date.now },
    },
    {
        versionKey: false
    }
);

ParticipantSchema.pre('save', next => {
    now = new Date();
    if(!this.createdAt) {
        this.createdAt = now;
    }
    next();
});

module.exports = mongoose.model('participant', ParticipantSchema);