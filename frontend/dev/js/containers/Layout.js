import React from 'react';

const Layout = (props) => (
    <main role="main" className="container">
        <div className="jumbotron">
            {props.children.map(child => child)}
        </div>
    </main>
);

export default Layout;
