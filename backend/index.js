const config = require('./config')
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const api = require('./app/routes/api/index')
mongoose.Promise = global.Promise;
mongoose.connect(config.dbUrl, { useMongoClient: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json'}));

app.get("/", (req, res) => res.json({message: "Welcome to "}));
app.use('/api', api)

app.listen(config.port);
console.log("Listening on port " + config.port);

module.exports = app
