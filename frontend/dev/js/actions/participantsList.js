export function participantsListFetchError(error) {
    return {
        type: 'PARTICIPANTS_LIST_ERROR',
        payload: {error}
    };
}

export function participantsListLoading() {
    return {
        type: 'PARTICIPANTS_LIST_LOADING',
    };
}

export function participantsListFetchSuccess(participantsList) {
    return {
        type: 'PARTICIPANTS_LIST_SUCCESS',
        payload: {participants: participantsList}
    };
}

export function participantsListFetch(url) {
    return (dispatch) => {
        dispatch(participantsListLoading());
        fetch(url)
            .then((response) => {
                console.log("ASDAsadas: ", response)
                if (!response.ok) {
                    dispatch(participantsListFetchError("Can't load data!"));
                }
                return response;
            })
            .then((response) => response.json())
            .then((items) => dispatch(participantsListFetchSuccess(items)))
            .catch(() => dispatch(participantsListFetchError("Can't load data!")));
    };
}