const {dbUrl, port} = require('../config');
const chai = require('chai');
const request = require('request');
const app = require('../index');

describe("Api tests", function () {
    const apiUrl = "http://localhost:" + port + "/api";
    it(`Fetch participants list`, function (done) {
        request.get(apiUrl + "/participants", function (err, res, body) {
            chai.expect(res.statusCode).to.equal(200);
            chai.expect(JSON.parse(body)).to.be.an('array');
            done()
        });
    });

    it(`Add new participant`, function (done) {
        request({
            url: apiUrl + "/participant",
            method: "POST",
            body: {
                FirstName: "Andrzej",
                LastName: "Wiśniewski",
                Email: "andrzej@wisniewski.pl",
                EventDate: new Date()
            },
            json: true,
        }, function (err, res, body) {
            chai.expect(res.statusCode).to.equal(200);
            chai.expect(body).to.be.an('object');
            chai.expect(body.participant.LastName).to.equal('Wiśniewski');
            done()
        });
    });

    it(`Edit participant`, function (done) {
        request.get(apiUrl + "/participants", function (err, res, body) {
            chai.expect(JSON.parse(body)).to.be.an('array');
            chai.expect(JSON.parse(body)).to.have.lengthOf.above(0);
            const _id = JSON.parse(body)[0]._id;
            chai.expect(typeof _id).to.be.a('string');
            request({
                url: apiUrl + "/participant/" + _id,
                method: "PUT",
                body: {LastName: "Jabłoński"},
                json: true,
            }, function (err, res, body) {
                chai.expect(res.statusCode).to.equal(200);
                chai.expect(body).to.be.an('object');

                request.get(apiUrl + "/participant/" + _id, function (err, res, body) {
                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(typeof body).to.be.an('string');
                    chai.expect(JSON.parse(body).LastName).to.equal('Jabłoński');
                    done()
                });
            });
        });
    });

    const data = [
        {FirstName: "", LastName: "Wiśniewski", Email: "andrzej@wisniewski.pl", EventDate: new Date()},
        {FirstName: "Andrzej", LastName: "", Email: "andrzej@wisniewski.pl", EventDate: new Date()},
        {FirstName: "Andrzej", LastName: "Wiśniewski", Email: "", EventDate: new Date()},
        {FirstName: "Andrzej", LastName: "Wiśniewski", Email: "andrzej", EventDate: new Date()},
        {FirstName: "Andrzej", LastName: "Wiśniewski", Email: "andrzej@wisniewski.pl", EventDate: ""},
    ];

    data.forEach(record => {
        it(`Check data validation for edit record: ${JSON.stringify(record)}`, function (done) {
            request.get(apiUrl + "/participants", function (err, res, body) {
                const _id = JSON.parse(body)[0]._id;
                request({
                    url: apiUrl + "/participant/" + _id,
                    method: "PUT",
                    body: record,
                    json: true,
                }, function (err, res, body) {
                    chai.expect(res.statusCode).to.equal(400);
                    done()
                });
            });
        })
    });

    data.forEach(record => {
        it(`Check data validation for adding record: ${JSON.stringify(record)}`, function (done) {
            request({
                url: apiUrl + "/participant",
                method: "POST",
                body: record,
                json: true,
            }, function (err, res, body) {
                chai.expect(res.statusCode).to.equal(400);
                done()
            });
        })
    })

});