export default function (state = {status:"", msg: "", participants: []}, action) {
    switch (action.type) {
        case 'PARTICIPANTS_LIST_LOADING':
            return {status:"LOADING", msg: "List loading", participants: [...state.participants]};
            break;
        case 'PARTICIPANTS_LIST_ERROR':
            return {status:"ERROR", msg: action.payload.error, participants: []};
            break;
        case 'PARTICIPANTS_LIST_SUCCESS':
            return {status:"SUCCESS", msg: "Participant Loaded", participants: action.payload.participants};
            break;
    }
    return state
}
