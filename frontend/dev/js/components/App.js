import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'
import Layout from '../containers/Layout'
import List from '../containers/List';
import Participant from '../containers/Participant';
require('../../scss/style.scss');

const App = () => (

    <Router>
        <div>
            <Layout>
                <Route exact path="/" component={List}/>
                <Route path="/participant/:id?" component={Participant}/>
            </Layout>
        </div>
    </Router>

);

export default App;
